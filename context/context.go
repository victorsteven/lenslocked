package context

import (
	"context"
	"lenslocked/models"
)

const (
	userKey privateKey = "user"
)
type privateKey string

//A function that returns a context with a user attached to it
func WithUser(ctx context.Context, user *models.User) context.Context {
	return context.WithValue(ctx, userKey, user)
}

//Get the user from the context
func User(ctx context.Context) *models.User {
	if temp := ctx.Value(userKey); temp != nil {
	//	try to convert the interface to the User struct
		if user, ok := temp.(*models.User); ok {
			return user
		}
	}
	return nil
}