package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"lenslocked/controllers"
	"lenslocked/middleware"
	"lenslocked/models"
	"log"
	"net/http"
)

//var (
//	homeView *views.View
//	contactView *views.View
//)

const (
	host = "127.0.0.1"
	port = "5432"
	dbname = "lenslocked_dev"
	password = ""
	user = "steven"
)

func main() {

	DBURL := fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=disable password=%s", host, port, user, dbname, password)
	services, err := models.NewServices(DBURL)
	if err != nil {
		panic(err)
	}
	defer services.Close()
	//services.DestructiveReset()
	services.Automigrate()
	//if err := us.Automigrate(); err != nil {
	//	panic(err)
	//}

	//homeView = views.NewView("bootstrap", "views/home.gohtml")
	//contactView = views.NewView("bootstrap", "views/contact.gohtml")
	//r.HandleFunc("/", home).Methods("GET")
	//r.HandleFunc("/contact", contact).Methods("GET")

	r := mux.NewRouter()

	staticC := controllers.NewStatic()
	usersC := controllers.NewUsers(services.User)
	galleriesC := controllers.NewGalleries(services.Gallery, services.Image, r)

	userMw := middleware.User{
		UserService: services.User,
	}
	//This is the user middleware
	requireUserMw := middleware.RequireUser{User: userMw}

	r.Handle("/", staticC.Home).Methods("GET")
	r.Handle("/contact", staticC.Contact).Methods("GET")

	r.HandleFunc("/signup", usersC.New).Methods("GET")
	r.HandleFunc("/signup", usersC.Create).Methods("POST")

	r.Handle("/login", usersC.LoginView).Methods("GET")
	r.HandleFunc("/login", usersC.Login).Methods("POST")
	//r.HandleFunc("/cookietest", usersC.CookieTest).Methods("GET")

	//Asset route
	//the dir path specified below is the path from where the app root resides
	assertHandler := http.FileServer(http.Dir("./assets/"))
	assertHandler = http.StripPrefix("/assets/", assertHandler)
	r.PathPrefix("/assets/").Handler(assertHandler)

	//Make the image appear on the screen
	imageHandler := http.FileServer(http.Dir("./images/"))
	r.PathPrefix("/images/").Handler(http.StripPrefix("/images/", imageHandler))

	//Gallery Routes
	//New is not a function. It is actually a view
	r.Handle("/galleries/new", requireUserMw.Apply(galleriesC.New)).Methods("GET")
	r.Handle("/galleries", requireUserMw.ApplyFn(galleriesC.Index)).Methods("GET")

	//Create is a function.
	r.HandleFunc("/galleries", requireUserMw.ApplyFn(galleriesC.Create)).Methods("POST")
	r.HandleFunc("/galleries/{id}/edit", requireUserMw.ApplyFn(galleriesC.Edit)).Methods("GET").Name(controllers.EditGallery)
	r.HandleFunc("/galleries/{id}/update", requireUserMw.ApplyFn(galleriesC.Update)).Methods("POST")
	r.HandleFunc("/galleries/{id}/delete", requireUserMw.ApplyFn(galleriesC.Delete)).Methods("POST")

	r.HandleFunc("/galleries/{id}/images", requireUserMw.ApplyFn(galleriesC.ImageUpload)).Methods("POST")
	r.HandleFunc("/galleries/{id}/images/{filename}/delete", requireUserMw.ApplyFn(galleriesC.ImageDelete)).Methods("POST")


	r.HandleFunc("/galleries/{id}", galleriesC.Show).Methods("GET").Name(controllers.ShowGallery)

	if err :=	http.ListenAndServe(":3000", userMw.Apply(r));  err != nil {
		log.Fatal("cannot start server")
	}
}

//func home(w http.ResponseWriter, r *http.Request) {
//	w.Header().Set("Content-Type", "text/html")
//	//fmt.Fprint(w, "<h1>This is the home page</h1>")
//	//if err := homeView.Template.ExecuteTemplate(w, homeView.Layout, nil); err != nil {
//	//	//if err := homeView.Template.Execute(w, nil); err != nil {
//	//	panic(err)
//	//}
//	must(homeView.Render(w, nil))
//}

//func contact(w http.ResponseWriter, r *http.Request) {
//	w.Header().Set("Content-Type", "text/html")
//	//fmt.Fprint(w, "To get in touch, send a mail to <a href=\"mailto:support@gmail.com\"> support mail</a>.")
//	//if err := contactView.Template.ExecuteTemplate(w, contactView.Layout, nil); err != nil {
//	//	//if err := contactView.Template.Execute(w, nil); err != nil {
//	//	panic(err)
//	//}
//	must(contactView.Render(w, nil))
//}

func must(err error) {
	if err != nil {
		panic(err)
	}
}



//func server(w http.ResponseWriter, r *http.Request) {
//	w.Header().Set("Content-Type", "text/html")
//	if r.URL.Path == "/" {
//		fmt.Fprint(w, "<h1> This is a simple server yes </h1>")
//	} else if r.URL.Path == "/contact" {
//		fmt.Fprint(w, "To get in touch, send a mail to <a href=\"mailto:support@gmail.com\"> support mail</a>.")
//	}
//}
