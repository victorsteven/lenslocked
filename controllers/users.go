package controllers

import (
	"lenslocked/models"
	"lenslocked/rand"
	"lenslocked/views"
	"log"
	"net/http"
)

type SignUpForm struct {
	Name string `schema:"name"`
	Email string `schema:"email"`
	Password string `schema:"password"`
}

func NewUsers(us models.UserService) *Users {
	return &Users{
		NewView: views.NewView("bootstrap", "users/new"),
		LoginView: views.NewView("bootstrap", "users/login"),
		us: us,
	}
}
type Users struct {
	NewView *views.View
	LoginView *views.View
	us models.UserService
}
//This renders the sign up form
func (u *Users) New(w http.ResponseWriter, r *http.Request) {

	//d := views.Data{
	//	Alert: &views.Alert{
	//		Level:   views.AlertLvlError,
	//		Message: "something went wrong",
	//	},
	//}
	//if err := u.NewView.Render(w, nil); err != nil {
	//	panic(err)
	//}
	u.NewView.Render(w, r,nil);
}

//Create a new user
func (u *Users) Create(w http.ResponseWriter, r *http.Request) {
	var vd views.Data
	var form SignUpForm
	if err := parseForm(r, &form); err != nil {
		//panic(err)
		log.Println(err)
		vd.SetAlert(err)
		vd.Alert = &views.Alert{
			Level:  views.AlertLvlError,
			Message: views.AlertMsgGeneric,
		}
		u.NewView.Render(w, r,  vd)
		return
	}
	user := models.User{
		Name: form.Name,
		Email: form.Email,
		Password: form.Password,
	}
	if err :=  u.us.Create(&user);  err != nil {
		vd.SetAlert(err)
		//vd.Alert = &views.Alert{
		//	Level: views.AlertLvlError,
		//	Message: err.Error(),
		//}
		u.NewView.Render(w, r,  vd)
		//http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	//The user above is not a pointer, so, we "convert it to a pointer"(we pass a reference to the user)
	err := u.signIn(w, &user)
	if err != nil {
		http.Redirect(w, r, "/login", http.StatusFound)
		//http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	http.Redirect(w, r, "/galleries", http.StatusFound)

	//if err := r.ParseForm(); err != nil {
	//	panic(err)
	//}
	//dec := schema.NewDecoder()
	//if err := dec.Decode(&form, r.PostForm); err != nil {
	//	panic(err)
	//}
	//fmt.Fprintln(w, form)
	//fmt.Fprintln(w, r.PostForm["email"])
	//fmt.Fprintln(w, r.PostFormValue("email"))
}

type LoginForm struct {
	Email string `schema:"email"`
	Password string `schema:"password"`
}

func (u *Users) Login(w http.ResponseWriter, r *http.Request) {
	vd := views.Data{}
	form := LoginForm{}
	if err := parseForm(r, &form); err != nil {
		//panic(err)
		log.Println(err)
		vd.SetAlert(err)
	//	when the login fails, redirect back to the login page
		u.LoginView.Render(w, r,  vd)
		return
	}

	user, err := u.us.Authenticate(form.Email, form.Password)
	if err != nil {
		switch err {
		case models.ErrNotFound:
			vd.AlertError("Invalid email address")
			//fmt.Fprintln(w, "Invalid email address")
		//case models.ErrPasswordIncorrect:
		//	fmt.Fprintln(w, "Invalid password provided")

		default:
			//http.Error(w, err.Error(), http.StatusInternalServerError)
			vd.SetAlert(err)
		}
		u.LoginView.Render(w, r,  vd)
		return
	}
	err = u.signIn(w, user)
	if err != nil {
		//http.Error(w, err.Error(), http.StatusInternalServerError)
		vd.SetAlert(err)
		u.LoginView.Render(w, r,  vd)
		return
	}
	http.Redirect(w, r, "/galleries", http.StatusFound)
}

func (u *Users) signIn(w http.ResponseWriter, user *models.User) error {
	if user.Remember == "" {
		token, err := rand.RememberToken()
		if err != nil {
			return err
		}
		user.Remember = token
		err = u.us.Update(user)
		if err != nil {
			return err
		}
	}
	cookie := http.Cookie{
		Name:       "remember_token",
		Value:      user.Remember,
		HttpOnly: 	true,
	}
	http.SetCookie(w, &cookie)
	return nil
}

//func (u *Users) CookieTest(w http.ResponseWriter, r *http.Request) {
//	cookie, err := r.Cookie("remember_token")
//	if err != nil {
//		http.Redirect(w, r, "/login", http.StatusFound)
//		//http.Error(w, err.Error(), http.StatusInternalServerError)
//		return
//	}
//	user, err := u.us.ByRemember(cookie.Value)
//	if err != nil {
//		http.Redirect(w, r, "/login", http.StatusFound)
//		//http.Error(w, err.Error(), http.StatusInternalServerError)
//		return
//	}
//	fmt.Fprintln(w, user)
//}

