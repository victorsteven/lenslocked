module lenslocked

go 1.13

require (
	github.com/gorilla/mux v1.7.3
	github.com/gorilla/schema v1.1.0
	github.com/jinzhu/gorm v1.9.11
	github.com/lib/pq v1.2.0 // indirect
	golang.org/x/crypto v0.0.0-20190325154230-a5d413f7728c
)
