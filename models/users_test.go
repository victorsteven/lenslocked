package models

import (
	"fmt"
	"testing"
	"time"
)

func testingUserService() (*UserService, error) {
	const (
		host = "127.0.0.1"
		port = "5432"
		dbname = "lenslocked_test "
		password = ""
		user = "steven"
	)
	DBURL := fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=disable password=%s", host, port, user, dbname, password)

	us, err := NewUserService(DBURL)
	if err != nil {
		return nil, err
	}
	us.db.LogMode(false)

	//	clear the users table between tests
	us.DestructiveReset()
	return us, nil
}

func TestCreateUser(t *testing.T) {
	us, err := testingUserService()
	if err != nil {
		t.Fatal(err)
	}
	user := User{
		Name: "ge",
		Email: "ge@gmail.com",
	}
	err = us.Create(&user)
	if err != nil {
		t.Fatal(err)
	}
	if user.ID == 0 {
		t.Errorf("Expected ID > 0. Received %d", user.ID)
	}
	//if the time the user was created is greater than 5 seconds
	if time.Since(user.CreatedAt) > time.Duration(5 *time.Second) {
		t.Errorf("Expected CreatedAt be recent. Received %s", user.CreatedAt)
	}

	if time.Since(user.UpdatedAt) > time.Duration(5 *time.Second) {
		t.Errorf("Expected UpdatedAt be recent. Received %s", user.UpdatedAt)
	}
 }