package models

import "strings"

const (
	ErrNotFound modelError = "models: resource not found"
	ErrInvalidID privateError = "models: ID provided was invalid"
	//ErrInvalidEmail modelError = "models: Incorrect email provided"
	ErrPasswordIncorrect modelError = "models: Incorrect password provided"

	//emailRegex = regexp.MustCompile(`^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$`)

	ErrEmailRequired modelError = "email address is required"
	ErrEmailInvalid modelError = "email address is not valid"
	ErrEmailTaken modelError = "models: email address is already taken"
	ErrPasswordTooShort modelError = "models: password must be 8 characters long"
	ErrPasswordRequired modelError = "models: password is required"
	ErrRememberTooShort privateError = "models: remember token must be at least 32 bytes"
	ErrRememberRequired privateError = "models: remember required"
	ErrUserIDRequired privateError = "models: User id is required"
	ErrTitleRequired modelError = "models: title is required"
)

type modelError string

//This function implements the error interface
func (e modelError) Error() string {
	return string(e)
}
func (e modelError) Public() string {
	s := strings.Replace(string(e), "models: ", "", 1)
	split := strings.Split(s, " ")
	split[0] =  strings.Title(split[0]) //replace only the first letter
	return strings.Join(split, " ") //join back all the strings
	//return strings.Title(s) //all the letters are capitalized here
}

//this is not a direct error caused by the user input, e.g, the userID of a user aught to be set before he can create a gallery
type privateError string

func (e privateError) Error() string {
	return string(e)
}