package main

import (
	"fmt"
	"lenslocked/models"
)

type User struct {
	Name string
	Map map[string]string
}

const (
	host = "127.0.0.1"
	port = "5432"
	dbname = "lenslocked_dev"
	password = ""
	user = "steven"
)
func main(){
	DBURL := fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=disable password=%s", host, port, user, dbname, password)
	us, err := models.NewUserService(DBURL)
	if err != nil {
		panic(err)
	}
	defer us.Close()
	us.DestructiveReset()

	user := models.User{
		Name: "steven",
		Email: "stevens@gmail.com",
		Password: "password",
		Remember: "abc123",
	}
	err = us.Create(&user)
	if err != nil {
		panic(err)
	}
	//fmt.Println(user)

	user2, err := us.ByRemember("abc123")

	if err != nil {
		panic(err)
	}
	//fmt.Println(user2)
	fmt.Println("THIS IS THE USER 2")
	fmt.Printf("%+v\n", *user2)


	//toHash := []byte("this is the string to hash")
	//h := hmac.New(sha256.New, []byte("my-secret-key"))
	//h.Write(toHash)
	//b := h.Sum(nil)
	//fmt.Println(base64.URLEncoding.EncodeToString(b))
	//hmac := hash.NewHMAC("my-secret-key")
	//fmt.Println(hmac.Hash("this is the string to hash"))
	//h.Reset()
	//h = hmac.New(sha256.New, []byte("new-my-secret-key"))
	//h.Write(toHash)
	//b = h.Sum(nil)
	//fmt.Println(b)


	//t, err := template.ParseFiles("hello.gohtml")
	//if err != nil {
	//	panic(err)
	//}
	//
	//data := User{
	//	Name: "Ade Mike",
	//	Map: map[string]string{
	//		"key1": "value1",
	//		"key2": "value2",
	//		"key3": "value3",
	//	},
	//}
	//
	////os.Stdout: print it to the terminal
	//err = t.Execute(os.Stdout, data)
	//if err != nil {
	//	panic(err)
	//}
	//
	////the html template package encode the script below, so it is harmless
	//data.Name = "Olu"
	//err = t.Execute(os.Stdout, data)
	//if err != nil {
	//	panic(err)
	//}
}
