package middleware

import (
	"lenslocked/context"
	"lenslocked/models"
	"net/http"
)

type User struct {
	models.UserService
}

//RequireUser assumes that User middleware  has already been run otherwise it will not work correctly
type RequireUser struct {
	User
}

func (mw *User) Apply(next http.Handler) http.HandlerFunc {
	return mw.ApplyFn(next.ServeHTTP)
}

func (mw *User) ApplyFn(next http.HandlerFunc) http.HandlerFunc {
	//the return below is a closure, it use the next variable that is defined outside of it.
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		cookie, err := r.Cookie("remember_token")
		if err != nil {
			next(w, r)
			return
		}
		user, err := mw.UserService.ByRemember(cookie.Value)
		if err != nil {
			next(w, r)
			return
		}
		//give us the current context from the request
		ctx := r.Context()
		//apply the user to that context
		ctx = context.WithUser(ctx, user)
		//update the request with the new context
		r  = r.WithContext(ctx)
		next(w, r)
	})
}
func (mw *RequireUser) Apply(next http.Handler) http.HandlerFunc {
	return mw.ApplyFn(next.ServeHTTP)
}

//RequireUser assumes that User middleware  has already been run otherwise it will not work correctly
func (mw *RequireUser) ApplyFn(next http.HandlerFunc) http.HandlerFunc {
	//the return below is a closure, it use the next variable that is defined outside of it.
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		user := context.User(r.Context())
		if user == nil {
			http.Redirect(w, r, "/login", http.StatusFound)
			return
		}
		next(w, r)
	})
	//	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	//	cookie, err := r.Cookie("remember_token")
	//	if err != nil {
	//		http.Redirect(w, r, "/login", http.StatusFound)
	//		//http.Error(w, err.Error(), http.StatusInternalServerError)
	//		return
	//	}
	//	user, err := mw.UserService.ByRemember(cookie.Value)
	//	if err != nil {
	//		http.Redirect(w, r, "/login", http.StatusFound)
	//		//http.Error(w, err.Error(), http.StatusInternalServerError)
	//		return
	//	}
	//	//give us the current context from the request
	//	ctx := r.Context()
	//	//apply the user to that context
	//	ctx = context.WithUser(ctx, user)
	//	//update the request with the new context
	//	r  = r.WithContext(ctx)
	//	fmt.Println("User found: ", user)
	//
	//	next(w, r)
	//})
}