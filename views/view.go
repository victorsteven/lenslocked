package views

import (
	"bytes"
	"fmt"
	"html/template"
	"io"
	"lenslocked/context"
	"log"
	"net/http"
	"path/filepath"
)

var (
	LayoutDir  = "views/layouts/"
	TemplateDir = "views/"
	TemplateExt = ".gohtml"
)
//We are not sure how many file string that will be passed, so we use a variadic paraameter
func NewView(layout string, files ...string) *View {
	addTemplatePath(files)
	addTemplateExt(files)
	fmt.Println(files)
	files = append(files, layoutFiles()...)
	//send the variadic parameter as strings
	t, err := template.ParseFiles(files...) //convert the file to a slice of string
	if err != nil {
		panic(err)
	}
	return &View{
		Template: t,
		Layout: layout,
	}
}

type View struct {
	Template *template.Template
	Layout string
}

func (v *View) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html")
	//if err := v.Render(w, nil); err != nil {
	//	panic(err)
	//}
	v.Render(w, r,nil);
}
//Render is used to render the view with the predefined layout
func (v *View) Render(w http.ResponseWriter, r *http.Request, data interface{})  {
	w.Header().Set("Content-Type", "text/html")
	var vd Data
	switch d := data.(type) {
	case Data:
		vd = d
	//	do nothing
	default:
		vd = Data{
			Yield: data,
		}
	}
	vd.User = context.User(r.Context())
	var buf bytes.Buffer
	//return v.Template.ExecuteTemplate(w, v.Layout, data)
	if err :=  v.Template.ExecuteTemplate(&buf, v.Layout, vd); err != nil {
		log.Println("Error rendering the template", err)
		http.Error(w, "Something went wrong.", http.StatusInternalServerError)
		return
	}
	//destination and the source
	io.Copy(w, &buf)
}

func layoutFiles() []string {
	files, err := filepath.Glob(LayoutDir + "*" + TemplateExt)
	if err != nil {
		panic(err)
	}
	return files
}

func addTemplatePath(files []string){
	for i, f := range files {
		files[i] = TemplateDir + f
	}
}

func addTemplateExt(files []string) {
	for i, f := range files {
		files[i] = f + TemplateExt
	}
}
