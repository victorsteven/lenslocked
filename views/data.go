package views

import (
	"lenslocked/models"
	"log"
)

const (
	AlertLvlError = "danger"
	AlertLvlWarning = "warning"
	AlertLvInfo = "info"
	AlertLvSuccess = "success"
	AlertMsgGeneric = "Something went wrong. Please contact us if problem persists"
)

type Alert struct {
	Level string
	Message string
}
type Data struct {
	Alert *Alert
	User *models.User
	Yield interface{}
}

func (d *Data) SetAlert(err error) {
//	if this error implements the "PublicError" interface
	if pErr, ok := err.(PublicError); ok {
		d.Alert = &Alert{
			Level:   AlertLvlError,
			Message: pErr.Public(),
		}
	} else {
		log.Println(err)
		d.Alert = &Alert{
			Level:   AlertLvlError,
			Message: AlertMsgGeneric,
		}
	}
}

func (d *Data) AlertError(msg string) {
	d.Alert = &Alert{
		Level:   AlertLvlError,
		Message: msg,
	}
}

type PublicError interface {
	error //same as Error() string
	Public() string
}