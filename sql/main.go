package main

import (
	"database/sql"
	"fmt"
	//"github.com/jinzhu/gorm"
	"lenslocked/models"
	"log"

	//_ "github.com/lib/pq"
	_ "github.com/jinzhu/gorm/dialects/postgres"

)


const (
	host = "127.0.0.1"
	port = "5432"
	dbname = "lenslocked_dev"
	password = ""
	user = "steven"
)

var db_postgres *sql.DB

//type User struct {
//	gorm.Model
//	Name string
//	Email string `gorm:"not null;unique_index"`
//}


func main(){
	DBURL := fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=disable password=%s", host, port, user, dbname, password)
	us, err := models.NewUserService(DBURL)
	if err != nil {
		panic(err)
	}
	defer us.Close()
	us.DestructiveReset()
	user := models.User{
		Name: "Steve Brown",
		Email: "stevend@gmail.com",
	}
	if err := us.Create(&user); err != nil {
		panic(err)
	}
	user.Email = "ojo@gmail.com"
	if err := us.Update(&user); err != nil {
		panic(err)
	}

	if err := us.Delete(user.ID); err != nil {
		panic(err)
	}
	//userByEmail, err := us.ByEmail(user.Email)
	//if err != nil {
	//	panic(err)
	//}
	//fmt.Println(userByEmail)

	userByID, err := us.ByID(user.ID)
	if err != nil {
		panic(err)
	}
	fmt.Println(userByID)

	//db.LogMode(true)
	//db.AutoMigrate(&User{})
	//var u User
	// if err := db.Where("email = ?", "agie@gmaol.com").First(&u).Error; err != nil {
	//	 switch err {
	//	 case gorm.ErrRecordNotFound:
	//		 fmt.Println("No user found!")
	//	 default:
	//		 panic(err)
	//	 }
	// }

	//errors := db.GetErrors()
	//if len(errors) > 0 {
	//	fmt.Println(errors)
	//	os.Exit(1)
	//}

	//var u User
	//db.Where("id = ?", 2).
	//	Where("name = ?", "mike").
	//	Where("email = ?", "mike@gmail.com").
	//	First(&u)

	//OR:

	//var u User = User{
	//	//	Name:  "mike",
	//	//	Email: "mike@gmail.com",
	//	//}
	//	//db.Where(u).First(&u)
	//	//fmt.Println(u)

	//Multiple records
	//var users []User
	//db.Find(&users)
	//fmt.Println(users)


	//name, email := getInfo()
	//u := User{
	//	Name: name,
	//	Email: email,
	//}
	//if err := db.Create(&u).Error; err != nil {
	//	panic(err)
	//}
	//fmt.Printf("%+\nv", u)
}

//func getInfo() (name, email string) {
//	reader := bufio.NewReader(os.Stdin)
//	fmt.Println("What is your name?")
//	name, _ = reader.ReadString('\n')
//	fmt.Println("What is your email address?")
//	email, _ = reader.ReadString('\n')
//	name = strings.TrimSpace(name)
//	email = strings.TrimSpace(email)
//	return name, email
//}

func postgresOpen() {
	var err error

	DBURL := fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=disable password=%s", host, port, user, dbname, password)

	db_postgres, err = sql.Open("postgres", DBURL)
	if err != nil {
		log.Fatal("This is the error connecting to postgres:", err)
	}
	defer db_postgres.Close()

	if err := db_postgres.Ping(); err != nil {
		fmt.Println("this is the db error", err)
	} else {
		fmt.Println("we are connected to the database")
	}
	//insertUser()
	//selectUser()
	//insertOrder()
	//selectUserAndOrder()
}

func insertUser(){
	var id int
	 _ = db_postgres.QueryRow(`
		INSERT INTO users(name, email)  VALUES($1, $2) RETURNING id`, "sam", "sam@gmail.com").Scan(&id)
	 fmt.Println("the inserted id: ", id)
}

func selectUser(){
	var err error
	var id int
	var name, email string
	row := db_postgres.QueryRow(`
		SELECT id, name, email FROM  users WHERE id=$1`, 1)
	err = row.Scan(&id, &name, &email)
	if err != nil {
		if err ==  sql.ErrNoRows {
			fmt.Println("no rows")
		} else {
			panic(err)
		}
	}
	fmt.Println("id", id, "name", name, "email", email)
}

func insertOrder(){
	for i := 1; i <= 6; i++ {
		userId := 1
		if i > 3 {
			userId = 3
		}
		amount := i * 100
		description := fmt.Sprintf("the data x%d", i)
		 _, err := db_postgres.Exec(`
		INSERT INTO orders(user_id, amount, description)  VALUES($1, $2, $3)`, userId, amount, description)
		if err != nil {
			panic(err)
		}

	}
}

func selectUserAndOrder(){
	rows, err := db_postgres.Query(`
			SELECT * FROM users 
			INNER JOIN orders ON users.id= orders.user_id`)
	if err != nil {
		panic(err)
	}
	for rows.Next() {
		var userId, orderId, amount int
		var email, name, desc string
		if err := rows.Scan(&userId, &name, &email, &orderId, &userId, &amount, &desc); err != nil {
			panic(err)
		}
		fmt.Println("userId", userId, "name: ", name, "email", email, "orderId: ", orderId, "amount: ", amount, "desc: ", desc)
	}
	if rows.Err() != nil {
		panic(rows.Err())
	}
}

